<?php

namespace App\Http\Controllers;

use App;
use PDF;
use Ramsey\Uuid\Uuid;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Invoice;
use App\InvoiceProduct;
use App\Customer;

class InvoiceController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function form()
    {
//        $userRole = DB::table('role_user')
//                    ->where('user_id', '=', Auth::user()->id)
//                    ->join('roles', 'role_user.role_id', '=', 'roles.id')
//                    ->select('roles.name')
//                    ->first();
        //echo json_encode($userRole->name);
        //echo(Auth::user());
        //die();
      if (Auth::user()->hasRole(['Owner', 'Admin']))
      {
        
        $usr=DB::table('users')->get();
        $rol=DB::table('roles')->get();
        $userRoles=DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'role_user.role_id', '=', 'roles.id')->select('users.*', 'roles.display_name', 'role_user.*')->get();

        return view('Users.Admin.dashboard')->with('data',$rol)->with('usrdata',$usr)->with('usrrol',$userRoles);
       }
        else 
        {
            return view('Users.Client.dashboard');
        }
    }

    public function index()
    {
        $invoices = Invoice::orderBy('created_at', 'desc')->where('user_id' , Auth::user()->userid)
            ->paginate(8);
       

        return view('invoices.index', compact('invoices'));
        
       
    }

    public function create()
    {
         $taxes=DB::table('taxes')->where('user_id' , Auth::user()->userid)->get();
         $customers= DB::table('customers')->where('user_id' , Auth::user()->userid)->get();

        return view('invoices.create')
        ->with(compact('customers'))
        ->with(compact('taxes'));
    }

    public function store(Request $request)
    {   

        // echo('here');
        // die();
        $invoiceid = Uuid::uuid4();
        $invoice_id = $invoiceid->toString();
        $userid=Auth::user()->userid;
        
        $this->validate($request, [
            'invoice_no' => 'required|alpha_dash|unique:invoices',
            'client_id' => 'required|max:255',
            'invoice_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'title' => 'required|max:255',
            'discount' => 'required|numeric|min:0',
            'products.*.name' => 'required|max:255',
            'products.*.price' => 'required|numeric|min:1',
            'products.*.qty' => 'required|integer|min:1'
        ]);
        
        if(!count($request->products)) {
            return response()
            ->json([
                'products_empty' => ['One or more Product is required.']
            ], 422);
        }

        $products = collect($request->products)->transform(function($product) {
            $product['total'] = $product['qty'] * $product['price'];
            $product['invoice_id'] ='temp invoice';
               return new InvoiceProduct($product);
        });

        foreach ($products as $p) {
            $p['invoice_id'] = $invoice_id;
        }
        $data = $request->except('products');
        
        $data['invoice_id']= $invoice_id;
        $data['user_id'] = $userid;
        $data['sub_total'] = $products->sum('total');
        $data['grand_total'] = $data['sub_total'] - $data['discount'];
//         try{
      
        $invoice = Invoice::create($data);
        $t=time();
        
        foreach ($products as $p) {
            // echo($p->invoice_id));
            // die();
            DB::table('invoice_products')->insert([
            'invoice_id'=>$p->invoice_id,
            'name'=>$p->name,
            'qty'=>$p->qty,
            'price'=>$p->price,
            'tax'=>$p->tax,
            'total'=>$p->total,
            'created_at'=>date('m/d/Y h:i:s a', $t)
            ]);
            //echo json_encode($product);
        }
        //die();
       
            //$invoice->products()->saveMany($products);
//        }
//        catch(Exception $e){
//            echo json_encode($e);
//            die();
//        }

        return response()
            ->json([
                'created' => true,
                'invoice_id' => $invoice->invoice_id
            ]);
    }

    public function show($invoice_id)
    {
        $invoice = Invoice::findByinvoiceidOrFail($invoice_id);
        $products = InvoiceProduct::findByinvoiceidOrFail($invoice_id);
        $invoice['products']=$products;
        return view('invoices.show', compact('invoice'));
    }

    public function edit($invoice_id)
    {
        $invoice = Invoice::findByinvoiceidOrFail($invoice_id);
        $products = InvoiceProduct::findByinvoiceidOrFail($invoice_id);
        $invoice['products']=$products;
        return view('invoices.edit', compact('invoice'));
    }

    public function update(Request $request, $invoice_id)
    {
        
    
        $this->validate($request, [
            'invoice_no' => 'required|alpha_dash|unique:invoices,invoice_no,'.$invoice_id.',invoice_id',
            'client_id' => 'required|max:255',
            'invoice_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'title' => 'required|max:255',
            'discount' => 'required|numeric|min:0',
            'products.*.name' => 'required|max:255',
            'products.*.price' => 'required|numeric|min:1',
            'products.*.qty' => 'required|integer|min:1'
        ]);

        $invoice = Invoice::findByinvoiceidOrFail($invoice_id);

        $products = collect($request->products)->transform(function($product) {
            $product['total'] = $product['qty'] * $product['price'];
            return new InvoiceProduct($product);
        });

       if(!count($request->products)) {
            return response()
            ->json([
                'products_empty' => ['One or more Product is required.']
            ], 422);
        }

        $data = $request->except('products');
        $data['sub_total'] = $products->sum('total');
        $data['grand_total'] = $data['sub_total'] - $data['discount'];

        $invoice->update($data);

        InvoiceProduct::where('invoice_id', $invoice->invoice_id)->delete();
        
        $t=time();
        

        foreach ($products as $p) {
            echo(json_encode($p));
            die();
            DB::table('invoice_products')->insert([
            'invoice_id'=>$p->invoice_id,
            'name'=>$p->name,
            'qty'=>$p->qty,
            'tax'=>$p->tax,
            'price'=>$p->price,
            'total'=>$p->total,
            // 'created_at'=>date("'m/d/Y h:i:s a'",$t),
            // 'updated_at'=>date("'m/d/Y h:i:s a'",$t)
            ]);
        }

        return response()
            ->json([
                'updated' => true,
                'invoice_id' => $invoice->invoice_id
            ]);
    }

    public function destroy($invoice_id)
    {
        $invoice = Invoice::findByinvoiceidOrFail($invoice_id);

        InvoiceProduct::where('invoice_id', $invoice->invoice_id)
            ->delete();

        $invoice->delete();

        return redirect()
            ->route('invoices.index');
    }
    
    public function printToPDF($invoice_id)
    {
//        $pdf = App::make('dompdf.wrapper');
//        $pdf->loadHTML('<h1>Test</h1>');
//        return $pdf->stream();
       
        $invoice = Invoice::findByinvoiceidOrFail($invoice_id);
        
        $products = InvoiceProduct::findByinvoiceidOrFail($invoice_id);
        
        $invoice['products']=$products;
        
        $pdf = PDF::loadView('invoices.invoice', compact('invoice'));

        return $pdf->stream();
        
        // return $pdf->download($invoice_id.'.pdf');
    }
    
}
