<?php

namespace App\Http\Controllers;

use App;
use PDF;
use Ramsey\Uuid\Uuid;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Invoice;
use App\Payment;
use App\Customer;

class PaymentController extends Controller
{
    

    public function __construct()
    {

        $this->middleware('auth');
    }


    public function index()
    {
         $payments = Payment::orderBy('created_at', 'desc')->where('user_id' , Auth::user()->userid);
         
         $customers = Customer::orderBy('created_at', 'desc')->where('user_id' , Auth::user()->userid);

          $invoices = Invoice::orderBy('created_at', 'desc')->where('user_id' , Auth::user()->userid);

        return view('payments.index' )
        ->with(compact('payments'))
        ->with(compact('customers'))
        ->with(compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
