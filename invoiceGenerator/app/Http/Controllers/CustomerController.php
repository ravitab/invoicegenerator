<?php

namespace App\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Customer;


class CustomerController extends Controller
{
    
    public function __construct()
    {

        $this->middleware('auth');
    }


    public function index()
    {
        $customers = Customer::orderBy('created_at', 'desc')->where('user_id' , Auth::user()->userid)
            ->paginate(8);

        return view('customers.index', compact('customers'));
    }

    public function create()
    {
        return view('customers.create');
    }

    public function store(Request $request)
    {
        $customerid = Uuid::uuid4();
        $userid=Auth::user()->userid;
       
        $this->validate($request, [
            'customer_name' => 'required|max:255',
            'customer_address' => 'required|max:255',
            'customer_ship_add' => 'required|max:255',
            'customer_email' => 'required|email|max:255',
            'description' => 'required|max:255',
            'mobile_no' => 'required|min:5|numeric',
            
        ]);
      
        
        $data = $request->all();
        $data['user_id']=$userid;
        $data['customer_id']=$customerid;
        $customer = Customer::create($data);

        return response()
            ->json([
                'created' => true,
                'id' => $customer->customer_id
            ]);
       
    }


    public function show($customer_id)
    {
        $customer = Customer::findBycustomeridOrFail($customer_id);
        return view('customers.show', compact('customer'));
    }

    public function edit($customer_id)
    {
        //$customer = DB::table('customers')->where('customer_id', $customer_id)->first();
        //$a= json_encode($customer);
        //return view('customers.edit', $customer);
        $customer = Customer::findBycustomeridOrFail($customer_id);
        return view('customers.show', compact('customer'));
    }


    public function update(Request $request, $customer_id)
    {
        $this->validate($request, [
            'customer_name' => 'required|max:255'.$id.',id',
            'customer_address' => 'required|max:255',
            'customer_ship_add' => 'required|max:255',
            'customer_email' => 'required|email|max:255|unique:customers',
            'description' => 'required|max:255',
            'mobile_no' => 'required|max:11',
          ]);

        $customer = Customer::findBycustomeridOrFail($customer_id);


        $customer->update($data);

        return response()
            ->json([
                'updated' => true,
                'id' => $customer->customer_id
            ]);
    }

    public function destroy($customer_id)
    {
        $customer = Customer::findBycustomeridOrFail($customer_id);
       
        $customer->delete();

        return redirect()
            ->route('customers.index');
    }
   

}

