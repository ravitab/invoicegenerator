<?php

namespace App\Http\Controllers;

use Ramsey\Uuid\Uuid;
use DB;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{

  use AuthenticatesAndRegistersUsers, ThrottlesLogins;
	

    public function __construct()
    {
        $this->middleware('auth');
    }  

    public function index()
    {
    	return view('auth.register');
    }


     public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        else {
            $this->create($request->all());
        }
   
            $rol=DB::table('roles')->get();
            $usr=DB::table('users')->get();
                
            $userRoles=DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')->select('users.*', 'roles.display_name', 'role_user.*')->get();

            return view('Users.Admin.dashboard')->with('data',$rol)->with('usrdata',$usr)->with('usrrol',$userRoles);
            
        
    }

       protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'address'=>'required|max:255',
            'password' => 'required|min:6|confirmed',
        ]);
    }

  protected function create(array $data)
    {
        $uuid4 = Uuid::uuid4();
         $user= User::create([
            'userid'=> $uuid4->toString(),
            'name' => $data['name'],
            'email' => $data['email'],
            'address'=>$data['address'],
            'password' => bcrypt($data['password']),
        ]);
      
     DB::table('role_user')->insert([
         'user_id'=>$user->id,
         'role_id'=>'2'
     ]);

     return $user;
    }


 
}
