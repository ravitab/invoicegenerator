<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::get('/dashboard', 'InvoiceController@form');
Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/register', 'UserController@index');
Route::post('register', 'UserController@Register');
Route::get('Updaterole', 'UserController@Updaterole');
Route::resource('invoices', 'InvoiceController');

Route::resource('customers', 'CustomerController');

Route::get('/taxes' , 'TaxController@index');
Route::get('/save' , 'TaxController@save');

Route::get('printToPDF/{invoice_id}', 'InvoiceController@printToPDF')->name('printToPDF');

 Route::resource('payments', 'PaymentController');




