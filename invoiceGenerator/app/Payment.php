<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'payment_method', 'payment_date', 'totalpayment', 'account_no', 'bankname' , 'cheque_no' , 
        'cheque_expdate', 'deposite_date'
    ];

    public function payment()
    {
        return $this->belongsTo(Invoice::class);
    }

   
    public static function findBypaymentidOrFail(
        $payment_id,
        $columns = array('*')
    ) {
        if ( ! is_null($payment= static::wherePaymentId($payment_id)->first($columns))) {
            return $payment;
        }

        throw new ModelNotFoundException;
    }
}
