<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Invoice Management</title>

    <!-- Bootstrap core CSS -->
    <link href="{{URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/css/bootstrap-reset.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{URL::asset('assets/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
     <link rel="stylesheet" href="{{URL::asset('assets/assets/data-tables/DT_bootstrap.css')}}" /> 
    <!--right slidebar-->
    <link href="{{URL::asset('assets/css/slidebars.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{URL::asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/css/style-responsive.css')}}" rel="stylesheet" />
   @stack('css')
<!--     <link rel="stylesheet" type="text/css" href="{{URL::asset('css/override.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/app.css')}}"> -->


  
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <header class="header white-bg">
          <div class="sidebar-toggle-box">
              <i class="fa fa-bars"></i>
          </div>
          <!--logo start-->
          <a href="#" class="logo" >Invoice<span>Management</span></a>
          <!--logo end-->
          
          <div class="top-nav ">
              <ul class="nav pull-right top-menu">
                  <li>
                      <input type="text" class="form-control search" placeholder="Search">
                  </li>
                  <!-- user login dropdown start-->
                  <li class="dropdown">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                          
                          <span class="username">                    <!-- Authentication Links -->
                       
                           
                                {{ Auth::user()->name }}
                         
                   
                   </span>
                          <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu extended logout">
                          <div class="log-arrow-up"></div>
                          <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                          <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                          <li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li>
                          
                          <li><a href="{{ url('/logout') }}"><i class="fa fa-key"></i> Log Out</a></li>
                      </ul>
                  </li>

                  <!-- user login dropdown end -->
                  <li class="sb-toggle-right">
                      <i class="fa  fa-align-right"></i>
                  </li>
              </ul>
          </div>
      </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                      <a href="{{ url('/dashboard') }}"">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="fa fa-laptop"></i>
                          <span>Customers</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{route('customers.create')}}">Add Customer</a></li>
                          <li><a  href="{{route('customers.index')}}">View Customers</a></li>
                          <li><a  href="#">Reports</a></li>
                          
                      </ul>
                  </li>

                 <!--  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="fa fa-book"></i>
                          <span>Products</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="#">Add Product</a></li>
                          <li><a  href="#">View Products</a></li>
                          <li><a  href="#">Reports</a></li>
                        
                      </ul>
                  </li>
 -->
                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="fa fa-cogs"></i>
                          <span>Invoice</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{route('invoices.create')}}">Generate Invoice</a></li>
                          <li><a  href="{{route('invoices.index')}}">View All</a></li>
                          <li><a  href="#">Reports</a></li>
                         
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Payments</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{route('payments.create')}}">Make Payment</a></li>
                          <li><a  href="{{route('payments.index')}}">View Payment</a></li>
                          <li><a  href="#">Reports</a></li>
                                               
                      </ul>
                  </li>      
                 

                  <!--multi level menu start-->
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-sitemap"></i>
                          <span>Settings</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="javascript:;">Profile Setting</a></li>
                          <li class="sub-menu">
                              <a  href="#">Bussiness Settings</a>
                              <ul class="sub">
                                  <li><a  href="{{ url('/taxes') }}">Tax Setting</a></li>
                                  <li><a  href="#">Settings 2.1</a></li>
                                  <li><a  href="#">Settings 2.1</a></li>
                                 
                              </ul>
                          </li>
                      </ul>
                  </li>
                  <!--multi level menu end-->

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
                <div class="contents"> @yield('content') </div>
              
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->


      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2016 &copy; Webninjaz.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

  


  </body>
      @stack('scripts')
</html>
