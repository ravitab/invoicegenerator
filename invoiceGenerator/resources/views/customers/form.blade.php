<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Customer Name</label>
            <input type="text" class="form-control" v-model="form.customer_name">
            <p v-if="errors.customer_name" class="error">@{{errors.customer_name[0]}}</p>
        </div>
        <div class="form-group">
            <label>Customer Address</label>
            <input type="text" class="form-control" v-model="form.customer_address">
            <p v-if="errors.customer_address" class="error">@{{errors.customer_address[0]}}</p>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Customer Shipping Address</label>
            <textarea class="form-control" v-model="form.customer_ship_add"></textarea>
            <p v-if="errors.customer_ship_add" class="error">@{{errors.customer_ship_add[0]}}</p>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Email ID</label>
            <input type="email" class="form-control" v-model="form.customer_email">
            <p v-if="errors.customer_email" class="error">@{{errors.customer_email[0]}}</p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <label>Description</label>
                <input type="text" class="form-control" v-model="form.description">
                <p v-if="errors.description" class="error">@{{errors.description[0]}}</p>
            </div>
            <div class="col-sm-6">
                <label>mobile_no</label>
                <input type="text" class="form-control" v-model="form.mobile_no">
                <p v-if="errors.mobile_no" class="error">@{{errors.mobile_no[0]}}</p>
            </div>
        </div>
    </div>
</div>
<hr>

