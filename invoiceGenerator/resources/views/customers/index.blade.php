@extends('layouts.client')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="clearfix">
                <span class="panel-title">Customers</span>
                <a href="{{route('customers.create')}}" class="btn btn-success pull-right">Create</a>
            </div>
        </div>
        <div class="panel-body">
            @if($customers->count())
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Email</th>
                        <th>Description</th>
                        <th>Mobile Number</th>
                        <th colspan="2">Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($customers as $customer)
                        <tr>
                            <td>{{$customer->customer_name}}</td>
                            <td>${{$customer->customer_address}}</td>
                            <td>{{$customer->customer_email}}</td>
                            <td>{{$customer->description}}</td>
                            <td>{{$customer->mobile_no}}</td>
                            <td>{{$customer->created_at->diffForHumans()}}</td>
                            <td class="text-right">
                             
                                <a href="{{route('customers.edit', $customer->customer_id)}}" class="btn btn-primary btn-sm">Edit</a>



                                <form class="form-inline" method="post"
                                    action="{{route('customers.destroy', $customer)}}"
                                    onsubmit="return confirm('Are you sure?')"
                                >
                                    <input type="hidden" name="_method" value="delete">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $customers->render() !!}
            @else
                <div class="invoice-empty">
                    <p class="invoice-empty-title">
                        No Customer were created.
                        <a href="{{route('customers.create')}}">Create Now!</a>
                    </p>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{URL::asset('assets/js/jquery.js')}}"></script>
    <script src="{{URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <script class="include" type="text/javascript" src="{{URL::asset('assets/js/jquery.dcjqaccordion.js')}}"></script>
    <script src="{{URL::asset('assets/js/jquery.scrollTo.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/slidebars.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/js/respond.min.js')}}" ></script>

    <!--common script for all pages-->
    <script src="{{URL::asset('assets/js/common-scripts.js')}}"></script>

@endpush