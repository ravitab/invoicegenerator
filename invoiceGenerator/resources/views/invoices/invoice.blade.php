    
    <!DOCTYPE html>
    <html>
    <head>


    <title>Invoice Management</title>
<link href="{{URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/css/bootstrap-reset.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{URL::asset('assets/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
     <link rel="stylesheet" href="{{URL::asset('assets/assets/data-tables/DT_bootstrap.css')}}" /> 
    <!--right slidebar-->
    <link href="{{URL::asset('assets/css/slidebars.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{URL::asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/css/style-responsive.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/override.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/app.css')}}">

    </head>
    <body>
            <header>

                    <h1 style="text-align:center;">Invoice</h1>

            </header>
         <div class="row">
            <div class="col-sm-4">
                    <div class="form-group">
                        <label>Invoice No.</label>
                        <p>{{$invoice->invoice_no}}</p>
                    </div>
                    <div class="form-group">
                        <label>Grand Total</label>
                        <p>{{$invoice->grand_total}}</p>
                    </div>
            </div>
            <div class="col-sm-4">
                    <div class="form-group">
                        <label>Client</label>
                        <p>{{$invoice->client}}</p>
                    </div>
                    <div class="form-group">
                        <label>Client Address</label>
                        <pre class="pre">{{$invoice->client_address}}</pre>
                    </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Title</label>
                    <p>{{$invoice->title}}</p>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label>Invoice Date</label>
                        <p>{{$invoice->invoice_date}}</p>
                    </div>
                    <div class="col-sm-6">
                        <label>Due Date</label>
                        <p>{{$invoice->due_date}}</p>
                    </div>
                </div>
             </div>
        </div>
            <hr>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($invoice->products as $product)
                        <tr>
                            <td class="table-name">{{$product->name}}</td>
                            <td class="table-price">{{$product->price}}</td>
                            <td class="table-qty">{{$product->qty}}</td>
                            <td class="table-total text-right">{{$product->qty * $product->price}}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Sub Total</td>
                        <td class="table-amount">{{$invoice->sub_total}}</td>
                    </tr>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Discount</td>
                        <td class="table-amount">{{$invoice->discount}}</td>
                    </tr>
                    <tr>
                        <td class="table-empty" colspan="2"></td>
                        <td class="table-label">Grand Total</td>
                        <td class="table-amount">{{$invoice->grand_total}}</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>


    
    </body>
    </html>



    