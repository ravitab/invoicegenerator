@extends('layouts.client')

@section('content')
<section class="panel">
                  <header class="panel-heading">
                     Taxes
                  </header>
                  <div class="panel-body">
                      <div class="adv-table editable-table ">
                          <div class="clearfix">
                              <div class="btn-group">
                                  <button id="editable-sample_new" class="btn green">
                                      Add New <i class="fa fa-plus"></i>
                                  </button>
                              </div>
                              <div class="btn-group pull-right">
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                  </button>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="#">Print</a></li>
                                      <li><a href="#">Save as PDF</a></li>
                                  </ul>
                              </div>
                          </div>
                          <div class="space15"></div>

                          <div class="table-responsive">

                          <table class="table table-striped table-hover table-bordered" id="editable-sample">
                              <thead>
                              <tr>
                                  <th>Id</th>
                                  <th>Tax Name</th>
                                  <th>Tax %</th>
                                  <th>Created At</th>
                                  <th>Edit</th>
                                  <th>Delete</th>
                              </tr>
                              </thead>
                              <tbody>
                               @foreach($taxes as $t)
                              <tr>
                                    <td>{{$t->id}}</td>
                                    <td>${{$t->name}}</td>
                                    <td>{{$t->tax}}</td>
                                    <td>{{$t->created_at}}</td>
                                    <td><a class="edit" href="javascript:;">Edit</a></td>
                                    <td><a class="delete" href="javascript:;">Delete</a></td> 
                              <!-- <tr class="">
                                  <td>Jondi Rose</td>
                                  <td>Alfred Jondi Rose</td>
                                  <td>1234</td>
                                  <td class="center">super user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td> -->
                              </tr> 
                              @endforeach
                              </tbody>
                          </table>

                          
                          </div>
                      </div>
                  </div>
</section>
   
@endsection

@push('scripts')
 <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="/assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/assets/js/jquery.dcjqaccordion.js"></script>
    <script src="/assets/js/jquery.scrollTo.min.js"></script>
    <script src="/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script type="text/javascript" src="/assets/assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/assets/assets/data-tables/DT_bootstrap.js"></script>
    <script src="/assets/js/respond.min.js" ></script>

  <!--right slidebar-->
  <script src="/assets/js/slidebars.min.js"></script>

    <!--common script for all pages-->
    <script src="/assets/js/common-scripts.js"></script>

      <!--script for this page only-->
      <script src="/assets/js/editable-table.js"></script>

      <!-- END JAVASCRIPTS -->
      <script>
          jQuery(document).ready(function() {
              EditableTable.init();
          });
      </script>
       <!-- js placed at the end of the document so the pages load faster -->
    

@endpush