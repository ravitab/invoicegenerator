var app = new Vue({
  el: '#invoice',
  data: {
    isProcessing: false,
    form: {},
    errors: {}
  },

  created: function () {
    Vue.set(this.$data, 'form', _form);
  },
  methods: {
    addLine: function() {
      this.form.products.push({name: '', price: 0, qty: 1});
    },
    remove: function(product) {
      this.form.products.$remove(product);
    },
    create: function() {
      this.isProcessing = true;
      
      this.$http.post('/invoices', this.form)
        .then(function(response) {
          
          if(response.data.created) {
            window.location = '/invoices/' + response.data.invoice_id;
          } else {
            this.isProcessing = false;
          }
        })
        .catch(function(response) {
          this.isProcessing = false;
          Vue.set(this.$data, 'errors', response.data);
           debugger;
        })
    },
    update: function() {
      this.isProcessing = true;
      this.$http.put('/invoices/' + this.form.invoice_id, this.form)
        .then(function(response) {
          if(response.data.updated) {
            window.location = '/invoices/' + response.data.invoice_id;
          } else {
            this.isProcessing = false;
          }
        })
        .catch(function(response) {
          this.isProcessing = false;
          Vue.set(this.$data, 'errors', response.data);
        })
    }
  },
  computed: {
    subTotal: function() {
      return this.form.products.reduce(function(carry, product) {

       if(parseFloat(product.tax)){

        return carry + ((parseFloat(product.qty) * parseFloat(product.price)) - ((parseFloat(product.qty)  * parseFloat(product.price) * parseFloat(product.tax))/100));
      }
      else
      {
          return carry + (parseFloat(product.qty) * parseFloat(product.price));
      }
      }, 0);
    },
    grandTotal: function() {
      return this.subTotal - parseFloat(this.form.discount);
    }
  }
})