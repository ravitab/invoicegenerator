var custapp = new Vue({
  el: '#customer',
  data: {
    isProcessing: false,
    form: {},
    errors: {}
  },
  created: function () {
    Vue.set(this.$data, 'form', _form);
  },
  methods: {
    create: function() {
      this.isProcessing = true;
    debugger;
      this.$http.post('/customers', this.form)
      
        .then(function(response) {
            debugger
          if(response.data.created) {
           
            window.location = '/customers/' + response.data.id;
          } else {
            this.isProcessing = false;
          }
        })
        .catch(function(response) {
          this.isProcessing = false;
          Vue.set(this.$data, 'errors', response.data);
        })
    },

    update: function() {
      this.isProcessing = true;
      this.$http.put('/customers/' + this.form.id, this.form)
        .then(function(response) {
          if(response.data.updated) {
            window.location = '/customers/' + response.data.id;
          } else {
            this.isProcessing = false;
          }
        })
        .catch(function(response) {
          this.isProcessing = false;
          Vue.set(this.$data, 'errors', response.data);
        })
    }
  }
  
})